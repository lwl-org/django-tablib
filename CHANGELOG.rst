CHANGELOG
=========

Unreleased
----------

3.3.1 (2024-08-06)
------------------

* use django provided function ``django.contrib.admin.ModelAdmin.get_changelist_instance`` instead of replicating it's code 


3.3.0 (2024-07-24)
------------------

* Add support for Django 4.0
* Replace usage of ``django.conf.urls.url`` with ``django.urls.re_path``
* Replace usage of ``ugettext_lazy`` and ``force_text``


3.2.1 (2024-03-11)
------------------

* change legacy ``aggregate_select`` to ``annotation_select``
* fix ``ChangeList`` missing attr issue
* drop support for Django<2


3.2 (2017-04-04)
----------------

* Experimental Django 1.10 support
* Drop support for Django<1.8
* Fix filename contains a comma "," bug
